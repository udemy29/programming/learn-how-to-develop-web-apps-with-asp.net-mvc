﻿Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations

Namespace Models.ViewModels

    Public Class LoginViewModel

        <Required>
        <DisplayName("ユーザ名")>
        Public Property UserName As String

        <Required>
        <DataType(DataType.Password)>
        <DisplayName("パスワード")>
        Public Property Password As String

    End Class

End Namespace