﻿@ModelType AAddressBookApp.Models.Address
@Code
    ViewData("Title") = "Delete"
    Layout = "~/Views/_LayoutPage1.vbhtml"
End Code

<h2>削除</h2>

<h3>削除してもよろしいですか？</h3>
<div>
    <h4>住所録</h4>
    <hr />
    <dl class="dl-horizontal">
        <dt>
            @Html.DisplayNameFor(Function(model) model.FullName)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.FullName)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Kana)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Kana)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.ZipCode)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.ZipCode)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Prefecture)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Prefecture)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.StreetAddress)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.StreetAddress)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Telephone)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Telephone)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Mail)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Mail)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Group.GroupName)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Group.GroupName)
        </dd>
    </dl>
    @Using (Html.BeginForm())
        @Html.AntiForgeryToken()

        @<div class="form-actions no-color">
            <input type="submit" value="削除" class="btn btn-default" /> |
            @Html.ActionLink("リストへ戻る", "Search")
        </div>
    End Using
</div>