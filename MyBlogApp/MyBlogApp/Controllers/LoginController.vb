﻿Imports MyBlogApp.Models.Providers
Imports MyBlogApp.Models.ViewModels

Namespace Controllers

    <AllowAnonymous>
    Public Class LoginController
        Inherits Controller

        Private ReadOnly membershipProvider As New CustomMembershipProvider

        ' GET: Login
        Function Index() As ActionResult
            Return View()
        End Function

        ' POST: Login
        <HttpPost>
        <ValidateAntiForgeryToken>
        Function Index(<Bind(Include:="UserName,Password")> model As LoginViewModel) As ActionResult
            If ModelState.IsValid Then
                If membershipProvider.ValidateUser(model.UserName, model.Password) Then
                    FormsAuthentication.SetAuthCookie(model.UserName, False)
                    Return RedirectToAction("Index", "Articles")
                End If
            End If
            Return View(model)
        End Function

        ' GET: Login/SignOut
        Function SignOut() As ActionResult
            FormsAuthentication.SignOut()
            Return RedirectToAction("Index", "Articles")
        End Function

    End Class

End Namespace