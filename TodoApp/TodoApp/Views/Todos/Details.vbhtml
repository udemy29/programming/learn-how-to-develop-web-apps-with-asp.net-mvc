﻿@Imports TodoApp.Models.Entities
@ModelType Todo
@Code
    ViewData("Title") = "Details"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<h2>詳細</h2>

<div>
    <h4>Todo</h4>
    <hr />
    <dl class="dl-horizontal">
        <dt>
            @Html.DisplayNameFor(Function(model) model.User.UserName)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.User.UserName)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Summary)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Summary)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Detail)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Detail)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.DueDate)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.DueDate)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.IsDone)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.IsDone)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.CreatedBy)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Creator)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.CreatedOn)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.CreatedOn)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.UpdatedBy)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Updater)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.UpdatedOn)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.UpdatedOn)
        </dd>
    </dl>
</div>
<p>
    @Html.ActionLink("編集", "Edit", New With {.id = Model.TodoId}) |
    @Html.ActionLink("リストへ戻る", "Index")
</p>