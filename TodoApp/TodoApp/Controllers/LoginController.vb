﻿Imports TodoApp.Models.Providers

Namespace Controllers

    <AllowAnonymous>
    Public Class LoginController
        Inherits Controller

        Private ReadOnly membershipProvider As New CustomMembershipProvider

        ' GET: Login
        Function Index() As ActionResult
            Return View()
        End Function

        <HttpPost()>
        <ValidateAntiForgeryToken()>
        Public Function Index(<Bind(Include:="UserName,Password")> model As LoginDto) As ActionResult
            If ModelState.IsValid Then
                If membershipProvider.ValidateUser(model.UserName, model.Password) Then
                    FormsAuthentication.SetAuthCookie(model.UserName, False)
                    Return RedirectToAction("Index", "Home")
                End If
            End If
            ViewBag.Message = "ログインに失敗しました。"
            Return View(model)
        End Function

        Public Function SignOut() As ActionResult
            FormsAuthentication.SignOut()
            Return RedirectToAction("Index")
        End Function

    End Class

End Namespace