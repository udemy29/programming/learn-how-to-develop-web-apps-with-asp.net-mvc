﻿@Imports TodoApp.Models.Entities
@ModelType Todo
@Code
    ViewData("Title") = "Delete"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<h2>削除</h2>

<h3>本当に削除してもよろしいですか？</h3>
<div>
    <h4>削除</h4>
    <hr />
    <dl class="dl-horizontal">
        <dt>
            @Html.DisplayNameFor(Function(model) model.User.UserName)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.User.UserName)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Summary)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Summary)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Detail)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Detail)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.DueDate)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.DueDate)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.IsDone)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.IsDone)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.CreatedBy)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.CreatedBy)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.CreatedOn)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.CreatedOn)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.UpdatedBy)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.UpdatedBy)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.UpdatedOn)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.UpdatedOn)
        </dd>

    </dl>
    @Using (Html.BeginForm())
        @Html.AntiForgeryToken()

        @<div class="form-actions no-color">
            <input type="submit" value="削除" class="btn btn-default" /> |
            @Html.ActionLink("リストへ戻る", "Index")
        </div>
    End Using
</div>
