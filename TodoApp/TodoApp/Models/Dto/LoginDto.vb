﻿Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations

Public Class LoginDto

    <DisplayName("ユーザ名")>
    <Required>
    Public Property UserName As String

    <DisplayName("パスワード")>
    <Required>
    <DataType(DataType.Password)>
    Public Property Password As String

End Class