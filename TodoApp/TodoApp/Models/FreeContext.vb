Imports System.Data.Entity
Imports TodoApp.Models.Entities

Partial Public Class FreeContext
    Inherits DbContext

    Public Sub New()
        MyBase.New("name=FreeContext")
        Database.SetInitializer(Of FreeContext)(Nothing)
        Database.Log = Sub(log) Console.WriteLine(log)
    End Sub

    Public Overridable Property Roles As DbSet(Of Role)
    Public Overridable Property Todos As DbSet(Of Todo)
    Public Overridable Property Users As DbSet(Of User)

    Protected Overrides Sub OnModelCreating(ByVal modelBuilder As DbModelBuilder)
        modelBuilder.HasDefaultSchema("FREE")

        ' UsersとRolesテーブルのリレーション（多対多）
        modelBuilder.Entity(Of User) _
            .HasMany(Function(u) u.Roles) _
            .WithMany(Function(r) r.Users) _
            .Map(Sub(ur)
                     ur.MapLeftKey("UserId")
                     ur.MapRightKey("RoleId")
                     ur.ToTable("UserRoles")
                 End Sub)
    End Sub

End Class