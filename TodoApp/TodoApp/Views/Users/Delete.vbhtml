﻿@ModelType TodoApp.Models.Entities.User
@Code
    ViewData("Title") = "Delete"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<h2>Delete</h2>

<h3>本当に削除してもよろしいですか？</h3>
<div>
    <h4>User</h4>
    <hr />
    <dl class="dl-horizontal">
        <dt>
            @Html.DisplayNameFor(Function(model) model.UserName)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.UserName)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Password)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Password)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.FamilyName)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.FamilyName)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.GivenName)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.GivenName)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.IsActive)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.IsActive)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.CreatedBy)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.CreatedBy)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.CreatedOn)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.CreatedOn)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.UpdatedBy)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.UpdatedBy)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.UpdatedOn)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.UpdatedOn)
        </dd>

    </dl>
    @Using (Html.BeginForm())
        @Html.AntiForgeryToken()

        @<div class="form-actions no-color">
            <input type="submit" value="Delete" class="btn btn-default" /> |
            @Html.ActionLink("リストへ戻る", "Index")
        </div>
    End Using
</div>
