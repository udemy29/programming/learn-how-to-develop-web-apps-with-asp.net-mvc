﻿@Imports TodoApp.Models.Entities
@ModelType IEnumerable(Of Todo)
@Code
    ViewData("Title") = "Index"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<h2>Todo一覧</h2>

<p>
    @Html.ActionLink("新規作成", "Create")
</p>
<table class="table">
    <tr>
        <th>
            @Html.DisplayNameFor(Function(model) model.User.UserName)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.Summary)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.Detail)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.DueDate)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.IsDone)
        </th>
        <th></th>
    </tr>

    @For Each item In Model
        @<tr>
            <td>
                @Html.DisplayFor(Function(modelItem) item.User.UserName)
            </td>
            <td>
                @Html.DisplayFor(Function(modelItem) item.Summary)
            </td>
            <td>
                @Html.DisplayFor(Function(modelItem) item.Detail)
            </td>
            <td>
                @Html.DisplayFor(Function(modelItem) item.DueDate)
            </td>
            <td>
                @Html.DisplayFor(Function(modelItem) item.IsDone)
            </td>
            <td>
                @Html.ActionLink("編集", "Edit", New With {.id = item.TodoId}) |
                @Html.ActionLink("詳細", "Details", New With {.id = item.TodoId}) |
                @Html.ActionLink("削除", "Delete", New With {.id = item.TodoId})
            </td>
        </tr>
    Next
</table>