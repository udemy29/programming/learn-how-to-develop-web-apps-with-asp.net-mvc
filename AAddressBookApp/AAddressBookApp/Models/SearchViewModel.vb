﻿Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations

Namespace Models

    Public Class SearchViewModel

        <DisplayName("カナ")>
        <RegularExpression("[ァ-ヶ]+")>
        Public Property Kana As String

        Public Property Addresses As List(Of Address)

    End Class

End Namespace