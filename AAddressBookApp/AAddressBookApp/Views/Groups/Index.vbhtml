﻿@ModelType IEnumerable(Of AAddressBookApp.Models.Group)
@Code
    ViewData("Title") = "グループ一覧"
    Layout = "~/Views/_LayoutPage1.vbhtml"
End Code

<h2>グループ一覧</h2>

<p>
    @Html.ActionLink("新規作成", "Create")
</p>
<table class="table">
    <tr>
        <th>
            @Html.DisplayNameFor(Function(model) model.GroupName)
        </th>
        <th></th>
    </tr>

@For Each item In Model
    @<tr>
        <td>
            @Html.DisplayFor(Function(modelItem) item.GroupName)
        </td>
        <td>
            @Html.ActionLink("編集", "Edit", New With {.id = item.GroupId}) |
            @Html.ActionLink("詳細", "Details", New With {.id = item.GroupId}) |
            @Html.ActionLink("削除", "Delete", New With {.id = item.GroupId})
        </td>
    </tr>
Next

</table>
