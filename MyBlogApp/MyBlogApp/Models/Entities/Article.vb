Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Namespace Models.Entities

    <Table("FREE.Articles")>
    Partial Public Class Article

        Public Sub New()
            Comments = New HashSet(Of Comment)()
        End Sub

        <Key>
        Public Property ArticleId As Integer

        <Required>
        <StringLength(50)>
        <DisplayName("タイトル")>
        Public Property Title As String

        <Required>
        <StringLength(2000)>
        <DisplayName("本文")>
        Public Property Body As String

        <DisplayName("投稿者")>
        Public Property CreatedBy As Integer

        <DisplayName("投稿日時")>
        Public Property CreatedOn As Date

        <DisplayName("更新者")>
        Public Property UpdatedBy As Integer

        <DisplayName("更新日時")>
        Public Property UpdatedOn As Date

        <DisplayName("カテゴリID")>
        Public Property CategoryId As Integer

        <NotMapped>
        <DisplayName("カテゴリ名")>
        Public Property CategoryName As String

        <ForeignKey("CategoryId")>
        Public Overridable Property Category As Category

        Public Overridable Property Comments As ICollection(Of Comment)
    End Class

End Namespace