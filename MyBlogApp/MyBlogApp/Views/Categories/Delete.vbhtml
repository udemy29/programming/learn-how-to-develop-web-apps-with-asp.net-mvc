﻿@ModelType MyBlogApp.Models.Entities.Category
@Code
    ViewData("Title") = "Delete"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<h2>Delete</h2>

<h3>このカテゴリを本当に削除してもよろしいですか？</h3>
<div>
    <h4>カテゴリ</h4>
    <hr />
    <dl class="dl-horizontal">
        <dt>
            @Html.DisplayNameFor(Function(model) model.CategoryName)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.CategoryName)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.ArticleCount)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.ArticleCount)
        </dd>
    </dl>
    @Using (Html.BeginForm())
        @Html.AntiForgeryToken()

        @<div class="form-actions no-color">
            <input type="submit" value="削除" class="btn btn-danger" /> |
            @Html.ActionLink("リストへ戻る", "Index")
        </div>
    End Using
</div>