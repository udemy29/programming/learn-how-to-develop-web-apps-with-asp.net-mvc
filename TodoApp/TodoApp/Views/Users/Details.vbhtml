﻿@ModelType TodoApp.Models.Entities.User
@Code
    ViewData("Title") = "Details"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<h2>詳細</h2>

<div>
    <h4>ユーザ</h4>
    <hr />
    <dl class="dl-horizontal">
        <dt>
            @Html.DisplayNameFor(Function(model) model.UserName)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.UserName)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Password)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Password)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.FamilyName)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.FamilyName)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.GivenName)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.GivenName)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.IsActive)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.IsActive)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Creator)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Creator)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.CreatedOn)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.CreatedOn)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Updater)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Updater)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.UpdatedOn)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.UpdatedOn)
        </dd>
    </dl>
</div>
<p>
    @Html.ActionLink("編集", "Edit", New With {.id = Model.UserId}) |
    @Html.ActionLink("リストへ戻る", "Index")
</p>