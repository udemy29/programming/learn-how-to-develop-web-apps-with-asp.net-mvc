﻿@ModelType IEnumerable(Of TodoApp.Models.Entities.User)
@Code
ViewData("Title") = "Index"
Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<h2>ユーザ一覧</h2>

<p>
    @Html.ActionLink("新規作成", "Create")
</p>
<table class="table">
    <tr>
        <th>
            @Html.DisplayNameFor(Function(model) model.UserName)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.Password)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.FamilyName)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.GivenName)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.IsActive)
        </th>
        <th></th>
    </tr>

@For Each item In Model
    @<tr>
        <td>
            @Html.DisplayFor(Function(modelItem) item.UserName)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.Password)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.FamilyName)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.GivenName)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.IsActive)
        </td>
        <td>
            @Html.ActionLink("編集", "Edit", New With {.id = item.UserId}) |
            @Html.ActionLink("詳細", "Details", New With {.id = item.UserId}) |
            @Html.ActionLink("削除", "Delete", New With {.id = item.UserId})
        </td>
    </tr>
Next

</table>
