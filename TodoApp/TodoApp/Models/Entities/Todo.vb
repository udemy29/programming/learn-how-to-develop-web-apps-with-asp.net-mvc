Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Namespace Models.Entities

    <Table("FREE.Todos")>
    Partial Public Class Todo

        <Key>
        Public Property TodoId As Integer

        <DisplayName("概要")>
        <Required>
        <StringLength(50)>
        Public Property Summary As String

        <DisplayName("詳細")>
        <StringLength(300)>
        Public Property Detail As String

        <DisplayName("期日")>
        Public Property DueDate As Date?

        <DisplayName("完了")>
        Public Property IsDone As Boolean

        <DisplayName("ユーザ")>
        Public Property UserId As Integer

        <DisplayName("作成者")>
        Public Property CreatedBy As Integer

        <DisplayName("作成者名")>
        <NotMapped>
        Public Property Creator As String

        <DisplayName("作成日時")>
        Public Property CreatedOn As Date

        <DisplayName("更新者")>
        Public Property UpdatedBy As Integer

        <DisplayName("更新者名")>
        <NotMapped>
        Public Property Updater As String

        <DisplayName("更新日時")>
        Public Property UpdatedOn As Date

        <ForeignKey("UserId")>
        Public Overridable Property User As User

    End Class

End Namespace